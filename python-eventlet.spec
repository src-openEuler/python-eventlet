%global _empty_manifest_terminate_build 0
Name:           python-eventlet
Version:        0.37.0
Release:        1
Summary:        Highly concurrent networking library
License:        MIT
URL:            http://eventlet.net
Source0:        https://files.pythonhosted.org/packages/source/e/eventlet/eventlet-%{version}.tar.gz
BuildArch:      noarch
%description
Eventlet is a concurrent networking library for Python that allows you to change how you run your code, not how you write it.

%package -n python3-eventlet
Summary:        Highly concurrent networking library
Provides:       python-eventlet
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling
BuildRequires:  python3-hatch-vcs
# General requires
BuildRequires:  python3-dns
BuildRequires:  python3-greenlet
BuildRequires:  python3-six
# General requires
Requires:       python3-dns
Requires:       python3-greenlet
Requires:       python3-six
%description -n python3-eventlet
Eventlet is a concurrent networking library for Python that allows you to change how you run your code, not how you write it.

%package help
Summary:        Highly concurrent networking library
Provides:       python3-eventlet-doc
%description help
Eventlet is a concurrent networking library for Python that allows you to change how you run your code, not how you write it.

%prep
%autosetup -n eventlet-%{version}

%build
%pyproject_build

%install
%pyproject_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi


%files -n python3-eventlet
%dir %{python3_sitelib}/
%{python3_sitelib}/eventlet*

%files help
%{_docdir}/*

%changelog
* Wed Oct 23 2024 caiyuxin <caiyuxin@kylinos.cn> - 0.37.0-1
- Update to 0.37.0
- Fix migration guide url

* Thu Aug 15 2024 liyanan <liyanan61@h-partners.com> - 0.36.1-2
- Fix import eventlet failed

* Sun Apr 07 2024 GuoCe <guoce@kylinso.cn> - 0.36.1-1
- Update to 0.36.1
- Fix OIDC authentication failure.
- Fix memory leak in greendns.
- Support awaiting GreenThread in an `async def` context.
- Add debug convenience helpers - asyncio, threads

* Sat May 06 2023 xu_ping <707078654@qq.com> - 0.33.3-1
- Update to 0.33.3

* Wed Aug 03 2022 liukuo <liukuo@kylinos.cn> - 0.33.1-1
- Update to 0.33.1

* Wed May 18 2022 OpenStack_SIG <openstack@openeuler.org> - 0.33.0-1
- Upgrade package python3-eventlet to version 0.33.0

* Mon Jul 26 2021 OpenStack_SIG <openstack@openeuler.org> - 0.30.2-1
- update to 0.30.2

* Fri Jan 15 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated

* Thu Mar 12 2020 zoushuangshuang <zoushuangshuang@huawei.com> - 0.23.0-3
- Package init

